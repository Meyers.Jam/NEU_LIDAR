function [pcloud_port, pcloud_star] = extract_clouds(bag_path,threshold,threshold_matrix)

register = rosbag(bag_path);


% Filter Rosbag by topic name
msg_velodyne_points_port = select(register, 'Topic', '/ns1/velodyne_points');
msg_velodyne_points_star = select(register, 'Topic', '/ns2/velodyne_points');
clear register


% Return messages as cell array
velodyne_points_port = readMessages(msg_velodyne_points_port);
clear msg_velodyne_points_port

velodyne_points_star = readMessages(msg_velodyne_points_star);
clear msg_velodyne_points_star


% Checking lengths of cell arrays and trimming if unequal
if length(velodyne_points_port) > length(velodyne_points_star)
    velodyne_points_port = velodyne_points_port(1:length(velodyne_points_star),1);
    
elseif length(velodyne_points_port) < length(velodyne_points_star)
    velodyne_points_star = velodyne_points_star(1:length(velodyne_points_port),1);
end


% Read ROS message to XYZ points
% Create function handle
readXYZ_handle = @readXYZ;

xyz_port = cellfun(readXYZ_handle,velodyne_points_port,'UniformOutput',false);
clear velodyne_points_port

xyz_star = cellfun(readXYZ_handle,velodyne_points_star,'UniformOutput',false);
clear velodyne_points_star readXYZ_handle


% Perform pointcloud thresholding if threshold value is passed
if exist('threshold','var')

    xyz_port = cellfun(@(x) crop(x,threshold), xyz_port,'UniformOutput',false);
    
    xyz_star = cellfun(@(x) crop(x,threshold), xyz_star,'UniformOutput',false);
    
end

% Perform pointcloud thresholding for square if threshold values are passed
if exist('threshold_matrix','var')

    xyz_port = cellfun(@(x) crop_cube(x,threshold_matrix), xyz_port,'UniformOutput',false);
    
    xyz_star = cellfun(@(x) crop_cube(x,threshold_matrix), xyz_star,'UniformOutput',false);
    
end


% Convert to Matlab pointCloud Structure
% Create function handle
pointCloud_handle = @pointCloud;

pcloud_port = cellfun(pointCloud_handle,xyz_port,'UniformOutput',false);
clear xyz_port

pcloud_star = cellfun(pointCloud_handle,xyz_star,'UniformOutput',false);
clear xyz_star pointCloud_handle

end