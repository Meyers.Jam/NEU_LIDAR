function [OT,bin_means,bin_covs] = merge_voxels(OT,bin_means,bin_covs,no_merge_count)

% This uses a quick and dirty recursive method for voxel merging by point
% properties. This was a first pass at implementing this method that has
% been left untouched for some time as we moved on to other methods. 


% Randomly select a voxel index containing points
bin_select = datasample(unique(OT.PointBins),1);

% Find K-nearest neighbors to voxel by voxel means
neighbors = knnsearch(bin_means,bin_means(bin_select,:),'K',11);

% Remove bin_select from neighbor list
neighbors = neighbors(2:11);

% Counter for the number of voxels to be merged
merge_count = 0;

% no_merge_count = Counter for the number of times no neighboring voxels 
% have met merging criteria


for i = 1:length(neighbors)
    
    % Calculate euclidian distance between selected voxel and neighbor
    euclid_dist = sqrt(sum((bin_means(bin_select)-bin_means(neighbors(i))).^2));
    
    % Calculate frobenius norm of selected voxel covariance and
    % neighbor voxel covariance
    frob_norm = sqrt(sum(sum((bin_covs(:,:,bin_select) - bin_covs(:,:,neighbors(i))).^2))); 
    
    if euclid_dist < 1 && frob_norm < .15
        
        % Increment voxel merging counter
        merge_count = merge_count + 1;
        
        % Retrieving points from selected voxel and neighbor i
        select_points = OT.Points((OT.PointBins == bin_select),:);
        neighbor_points = OT.Points((OT.PointBins == neighbors(i)),:);
        
        % Concatenating points to form merged voxel point array
        new_points = [select_points; neighbor_points];
        
        % Counting number of points in merged voxel
        point_count = size(new_points);
        point_count = point_count(1);
        
        % Calculating merged voxel mean
        mean_update = (sum(select_points,1) + sum(neighbor_points,1))...
                       / point_count;
                   
        % Update voxel means data structure
        bin_means(bin_select,:) = mean_update;
        
        % Calculate covariance
        cov_update = calc_cov(mean_update,new_points);
        
        % Update covariance data structure
        bin_covs(:,:,bin_select) = cov_update;
            
        % Updating PointBins data structure
        OT.PointBins(OT.PointBins == neighbors(i)) = bin_select;

        % Updating BinCount
        OT.BinCount = OT.BinCount - 1;
        
        % Updating BinBoundaries
        OT.BinBoundaries(bin_select,:) = [min(min(OT.BinBoundaries(bin_select,1:2), OT.BinBoundaries(neighbors(i),1:2))), ...
                                          max(max(OT.BinBoundaries(bin_select,1:2), OT.BinBoundaries(neighbors(i),1:2))), ...
                                          min(min(OT.BinBoundaries(bin_select,3:4), OT.BinBoundaries(neighbors(i),3:4))), ...
                                          max(max(OT.BinBoundaries(bin_select,3:4), OT.BinBoundaries(neighbors(i),3:4))), ...
                                          min(min(OT.BinBoundaries(bin_select,5:6), OT.BinBoundaries(neighbors(i),5:6))), ...
                                          max(max(OT.BinBoundaries(bin_select,5:6), OT.BinBoundaries(neighbors(i),5:6)))];
                                      
    end  
    
end 

if merge_count > 0
    
    [OT,bin_means,bin_covs] = merge_voxels(OT,bin_means,bin_covs,no_merge_count);
    
else 
    
    no_merge_count = no_merge_count + 1;
    
end

if no_merge_count < 100
    
    [OT,bin_means,bin_covs] = merge_voxels(OT,bin_means,bin_covs,no_merge_count);
    
end

end