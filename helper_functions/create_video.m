% NOTE: This is a scripts found online and lightly edited to read images
% from a directory and stitch them together into a video. 


% Make an avi movie from a collection of PNG images in a folder.

% Specify the folder.
myFolder = '../cloud_frames';
if ~isdir(myFolder)
    errorMessage = sprintf('Error: The following folder does not exist:\n%s', myFolder);
    uiwait(warndlg(errorMessage));
    return;
end

% Get a directory listing.
filePattern = fullfile(myFolder, '*.png');
pngFiles = dir(filePattern);

[~, reindex] = sort( str2double( regexp( {pngFiles.name}, '\d+', 'match', 'once' )));
pngFiles = pngFiles(reindex);

% Open the video writer object.
writerObj = VideoWriter('../NewburyStopped_1.avi');
open(writerObj);

% Go through image by image writing it out to the AVI file.
for frameNumber = 1 : length(pngFiles)
    % Construct the full filename.
    baseFileName = pngFiles(frameNumber).name;
    fullFileName = fullfile(myFolder, baseFileName);
    % Display image name in the command window.
    fprintf(1, 'Now reading %s\n', fullFileName);
    % Display image in an axes control.
    thisimage = imread(fullFileName);
    % Write this frame out to the AVI file.
    writeVideo(writerObj, thisimage);
end

% Close down the video writer object to finish the file.
close(writerObj);
