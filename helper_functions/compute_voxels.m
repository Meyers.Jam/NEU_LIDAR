[cloud_port,cloud_star] = extract_clouds('data/Stationary_square_2018-03-25-12-06-57.bag',20);

cloud_reg = register_clouds(cloud_port,cloud_star);

frame = 1;

%% Visualize Cloud


pcshow(cloud_reg{frame}.Location,'red');

%% Play Cloud Video

play_clouds(cloud_reg,0.05)

%% Create Octatree


OT = OcTree(cloud_reg{frame}.Location,'binCapacity',10);%,'style','weighted');        


%% Shrinking voxels to tightly bound points

OT.shrink;


%% Delete All Non-Leaf Voxels

% Remove all non-leaf voxels
OT.BinBoundaries = OT.BinBoundaries(unique(sort(OT.PointBins)),:);
% Update the bin count
OT.BinCount = length(OT.BinBoundaries);


%% Plotting Voxels

figure()
boxH = OT.plot;
cols = lines(OT.BinCount);
doplot3 = @(p,varargin)plot3(p(:,1),p(:,2),p(:,3),varargin{:});
for i = 1:OT.BinCount
    set(boxH(i),'Color',cols(i,:),'LineWidth', 1+OT.BinDepths(i))
    doplot3(cloud_reg{frame}.Location(OT.PointBins==i,:),'.','Color',cols(i,:))
end
axis image, view(3)


