
%% Load a sequence of point clouds.
clear;
clc;

[cloud_port,cloud_star] = extract_clouds('../data_collection/Stationary_peoplewalkrun_2018-03-25-12-15-13.bag');
cloud_reg = register_clouds(cloud_port,cloud_star);
pcloud = cloud_reg;

%%  Initialize parameters
addpath '../ocTree'
% Set the region of interest.
xlimits = [-10, 12];
ylimits = [-10, 10];
zlimits = [-2, 4];
roi = [xlimits; ylimits; zlimits];
% Set parameters for Ground plane segmentation
maxDistance = 0.1; % in meters
referenceVector = [0, 0, 1];
sensorLocation   = [0,0,0]; % place the Lidar sensor at the center of coordinate system
radius      = 2.6; % in meters

%% Ground & Ego-vehicle Segmentation
% Define PLayer
player = pcplayer(xlimits, ylimits, zlimits);
% Plot the original motion
pcNoneVehicle = cell(length(pcloud), 1);
for k = 1:length(pcloud)
    pc = pcloud{k}; 
    % Find interesting region
    indices = findPointsInROI(pc, roi);
    pc = select(pc, indices);
    % Remove Ground Plane
    [~, ~, outliers] = pcfitplane(pc, maxDistance, referenceVector);
    pcWithoutGround = select(pc, outliers);
    % Remove ego vehicle
    [nearIndices, ~] = findNeighborsInRadius(pcWithoutGround, sensorLocation, radius);
    outliers(nearIndices) = [];
    pcNoneVehicle{k} = select(pc, outliers);
    % View the player
    view(player, pcNoneVehicle{k})
end

%% Octree - calculate OT
for k = 1:size(pcloud,1)
    OT(k)=OcTree(pcNoneVehicle{k}.Location,'binCapacity',100,'style','weighted');    
    OT(k).shrink;
end
1
%% Octree - Split by movement
threshold = 30;
for frame_cur = 2:size(pcloud,1)
    frame_prev = frame_cur - 1;
    for i = OT(frame_cur).BinCount:-1:1
    %    num_cur = sum(OT(130).PointBins == i);
       bound = OT(frame_cur).BinBoundaries(i, :);
       bound_re = reshape(bound, [3 2]);
       indices_cur = findPointsInROI(pcNoneVehicle{frame_cur}, bound_re);
       indices_prev = findPointsInROI(pcNoneVehicle{frame_prev}, bound_re);
       norm_para = prod(bound_re(:,2)-bound_re(:,1));
       diff = (length(indices_cur) - length(indices_prev))/norm_para;
       if diff < threshold
           OT(frame_cur).BinBoundaries(i, :) = [];
           OT(frame_cur).BinCount = OT(frame_cur).BinCount - 1;
       end
    %    pcNoneVehicle{129} = select(pcNoneVehicle{129}, indices);
    end
end
2
%% Octree - Merge
for frame = 1:size(pcloud,1)
    for i = 1:20000
        rndnum = randperm(OT(frame).BinCount, 2);
        bound1 = OT(frame).BinBoundaries(rndnum(1), :);
        bound2 = OT(frame).BinBoundaries(rndnum(2), :);
        size_diff = sqrt(sum((bound1-bound2).^2));
        center_diff = sqrt(sum((sum(reshape(bound1, [3 2]),2)/2 - sum(reshape(bound2, [3 2]),2)/2).^2));
        if center_diff < 1.6 && size_diff < 2
            minX = min(bound1(1), bound2(1));
            minY = min(bound1(2), bound2(2));
            minZ = min(bound1(3), bound2(3));
            maxX = max(bound1(4), bound2(4));
            maxY = max(bound1(5), bound2(5));
            maxZ = max(bound1(6), bound2(6));
            OT(frame).BinBoundaries(rndnum(1), :) = [minX, minY, minZ, maxX, maxY, maxZ];
            OT(frame).BinBoundaries(rndnum(2), :) = [];
            OT(frame).BinCount = OT(frame).BinCount - 1;
        end
    end    
end
3

%% Octree - Show
% %WARNING: Cannot get to display at full speed (very slow). To exit, press CTRL+C in
% %Command Window
figure
hold on
for n=1:size(pcloud,1)
%     size(pcNoneVehicle,1)
    plot3(OT(n));
    pcshow(pcNoneVehicle{n}.Location,'MarkerSize',50)
    view(3);
    pause(1);
    clf;
end
