function play_clouds(cloud_cell,delay)
%play_clouds plays an array of point cloud structures using pcplayer
%function.
%  cloud_cell: array of multiple point cloud structures.
%  delay: amount of time in between image displays (seconds).

% Compute proper x-y limits to ensure no clipping.
maxLimit = max(abs([cloud_cell{1}.XLimits cloud_cell{1}.YLimits]));

xlimits = [-maxLimit maxLimit];
ylimits = [-maxLimit maxLimit];
zlimits = cloud_cell{1}.ZLimits;

% Create the player
player = pcplayer(xlimits, ylimits, zlimits);

% Customize player axis labels
xlabel(player.Axes, 'X (m)');
ylabel(player.Axes, 'Y (m)');
zlabel(player.Axes, 'Z (m)');

for i = 1:size(cloud_cell,1)
    ptCloud = cloud_cell{i};
    view(player, ptCloud);
    pause(delay); %Adjust to modify playback speed
end
end

