%kmeans_lidar
%Imports rosbag data collected from NEU Autonomous Car via extract_clouds
%function, then registers points via register_clouds function. Pointclouds
%are then formed into a designated number of clusters "K" using matlab's
%kmeans function. These clusters are matched from frame to frame to track
%objects over time. This function still lacks flexibility in tracking
%objects for any number of clusters different than K=5
%02APR18

%% Import lidar data from rosbags for two seperate lidars and register
% points via helper functions
threshold_matrix=[-10,10;-10,10;-1.4,2]; %Eliminate Ground Plane (Stationary)
%threshold_matrix=[-50,50;-50,50;-1.4,20]; %Eliminate Ground Plane (PeopleWalkingRunning)
[cloud_port,cloud_star] = extract_clouds('../data_collection/Stationary_square_2018-03-25-12-06-57.bag',10,threshold_matrix);
%[cloud_port,cloud_star] = extract_clouds('../data_collection/Stationary_squaremove_2018-03-25-12-10-14.bag',10,threshold_matrix);
%[cloud_port,cloud_star] = extract_clouds('../data_collection/Stationary_peoplewalkrun_2018-03-25-12-15-13.bag',40,threshold_matrix);
%[cloud_port,cloud_star] = extract_clouds('../data_collection/Stationary_square_2018-03-25-12-06-57.bag');
%[cloud_port,cloud_star] = extract_clouds('../data_collection/Stationary_squaremove_2018-03-25-12-10-14.bag');
%[cloud_port,cloud_star] = extract_clouds('../data_collection/Stationary_peoplewalkrun_2018-03-25-12-15-13.bag');
cloud_reg = register_clouds(cloud_port,cloud_star);
clear cloud_port cloud_star

%% Make Lidar data same format as examples
time=num2cell(1:1:size(cloud_reg,1))'; %Need actual timestamps from rosbag still
d.LidarPointCloud = struct('timestamp',time,'ptCloud',cloud_reg);
%pcloud = d.LidarPointCloud;

%% Create structure of cluster for every point cloud in time
K=5; %number of clusters
clear locations cluster X Y Z test x y z testeroo IDK
locations(1:K)=struct('cluster',[],'avg',[]); %Structure of clusters
cluster(1:size(d.LidarPointCloud,1))=struct('timestamp',time,'locations',locations);
figure
%pause;
for j=1:size(d.LidarPointCloud,1)
    clf;
    test=d.LidarPointCloud(j).ptCloud;
    X=test.Location(:,1);
    Y=test.Location(:,2);
    Z=test.Location(:,3);
    testeroo=[X,Y,Z];
    
    % kmeans
    test=d.LidarPointCloud.ptCloud; %Single Pointcloud from set
    IDK = kmeans(testeroo,K);
    
    % Seperate Clusters for each frame
    for n=1:K
        for i=1:size(IDK,1)
            if IDK(i)==n
                cluster(j).locations(n).cluster=cat(1,cluster(j).locations(n).cluster,[X(i),Y(i),Z(i)]);
            end
        end
    end
    
    % Find average point of each cluster
    for n=1:K
        cluster(j).locations(n).avg(1)=mean(cluster(j).locations(n).cluster(:,1));
        x(n)=mean(cluster(j).locations(n).cluster(:,1));
        cluster(j).locations(n).avg(2)=mean(cluster(j).locations(n).cluster(:,2));
        y(n)=mean(cluster(j).locations(n).cluster(:,2));
        cluster(j).locations(n).avg(3)=mean(cluster(j).locations(n).cluster(:,3));
        z(n)=mean(cluster(j).locations(n).cluster(:,3));
    end
    
    % Plot kmeans results
    s=ones(size(IDK,1),1);
    scatter3(X,Y,Z,s,IDK);
    hold on
    scatter3(x,y,z);
    view(-54,64);
    pause(0.0001);
end

%% Match clusters from frame to frame to track objects

%Create empty structure of objects same size as number of clusters
clear location objects
location=struct('timestamp',time,'avg',[],'next',[]); %Structure of locations
objects(1:K)=struct('location',location);

%% Initialize locations of objects with cluster avg data
for n=1:K
    objects(n).location(1).avg(1)=cluster(1).locations(n).avg(1);
    objects(n).location(1).avg(2)=cluster(1).locations(n).avg(2);
    objects(n).location(1).avg(3)=cluster(1).locations(n).avg(3);
end

%Find closest distance from each cluster center to all clusters in next frame
for i=1:K
    for j=2:length(d.LidarPointCloud)
        for n=1:K
            distance(n)=norm(objects(i).location(j-1).avg-cluster(j).locations(n).avg);
        end
        [D,I]=min(distance);
        if D<0.2 %If distance is too great, must be an anomoly, assume object was in last position
            objects(i).location(j).avg=cluster(j).locations(I).avg;
        else
            objects(i).location(j).avg=objects(i).location(j-1).avg;
        end
    end
end

%% Plot trajectories of objects

%object 1
for n=1:length(objects(1).location)
    x1(n)=objects(1).location(n).avg(1);
    y1(n)=objects(1).location(n).avg(2);
    z1(n)=objects(1).location(n).avg(3);
end

%object 2
for n=1:length(objects(2).location)
    x2(n)=objects(2).location(n).avg(1);
    y2(n)=objects(2).location(n).avg(2);
    z2(n)=objects(2).location(n).avg(3);
end

%object 3
for n=1:length(objects(3).location)
    x3(n)=objects(3).location(n).avg(1);
    y3(n)=objects(3).location(n).avg(2);
    z3(n)=objects(3).location(n).avg(3);
end

%object 4
for n=1:length(objects(4).location)
    x4(n)=objects(4).location(n).avg(1);
    y4(n)=objects(4).location(n).avg(2);
    z4(n)=objects(4).location(n).avg(3);
end

%object 5
for n=1:length(objects(5).location)
    x5(n)=objects(5).location(n).avg(1);
    y5(n)=objects(5).location(n).avg(2);
    z5(n)=objects(5).location(n).avg(3);
end

%% Plot paths of tracked objects
figure
hold on
%view(3);
title('Paths of tracked objects')
xlabel('x (meters)');
ylabel('y (meters)');
zlabel('z (meters)');
plot3(x1,y1,z1,'linewidth',5);
plot3(x2,y2,z2,'linewidth',5);
plot3(x3,y3,z3,'linewidth',5);
plot3(x4,y4,z4,'linewidth',5);
plot3(x5,y5,z5,'linewidth',5);
%scatter3(X,Y,Z,s,IDK); %last frame of pointclouds
legend('object1','object2','object3','object4','object5','clusters');

