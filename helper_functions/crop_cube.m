function [xyz] = crop_cube(xyz,threshold_matrix)

x1=threshold_matrix(1,1);
x2=threshold_matrix(1,2);
y1=threshold_matrix(2,1);
y2=threshold_matrix(2,2);
z1=threshold_matrix(3,1);
z2=threshold_matrix(3,2);
car=2; %eliminate points within distance of sensor/car

% Filter out xyz points outside thresholds
%x1=lowerbound of x
%x2=upperbound of x
%y1=lowerbound of y
%y2=upperbound of y
%z1=lowerbound of z
%z2=upperbound of z

% Find points outside threshold in X
filter_x = (xyz(:,1) > x2) | (xyz(:,1) < x1) | (xyz(:,1) < car);

% Find points outside threshold in Y
filter_y = (xyz(:,2) > y2) | (xyz(:,2) < y1) | (xyz(:,1) < car);

% Find points outside threshold in Z
filter_z = (xyz(:,3) > z2) | (xyz(:,3) < z1) | (xyz(:,1) < car);

% Find points outside threshold in X or Y
filter = filter_x | filter_y | filter_z;

% Remove points outside threshold in X or Y
xyz(filter,:) = [];

end
