% Extract port and starboard velodyne clouds from bag file in cell array of
% matlab pointCloud objects
[cloud_port,cloud_star] = extract_clouds('../data_collection/Stationary_peoplewalkrun_2018-03-25-12-15-13.bag',20);

% Register port and starboard pointclouds 
cloud_reg = register_clouds(cloud_port,cloud_star);

% Define frame of interest
frame = 1;

% Segment ground plane
maxDistance = 0.2; % in meters
referenceVector = [0, 0, 1];
[~, inPlanePointIndices, outliers] = pcfitplane(cloud_reg{frame}, maxDistance, referenceVector);
cloud = select(cloud_reg{frame},outliers);
cloud_reg{frame} = cloud;

% Free memory
clear cloud_port cloud_star


%% Visualize Cloud

fig = figure;set(fig, 'Visible', 'on');
pcshow(cloud_reg{frame}.Location,'red');


%% Play Cloud Video

play_clouds(cloud_reg,0.05)


%% Create Octatree

OT = OcTree(cloud_reg{frame}.Location,'binCapacity',200);%,'style','weighted');        

% Shrinking voxels to tightly bound points
OT.shrink;

% Remove all non-leaf voxels
OT.BinBoundaries = OT.BinBoundaries(unique(sort(OT.PointBins)),:);

% Update the bin count
OT.BinCount = length(OT.BinBoundaries);


%% Plotting Voxels

figure()
boxH = OT.plot;
cols = lines(OT.BinCount);
doplot3 = @(p,varargin)plot3(p(:,1),p(:,2),p(:,3),varargin{:});
for i = 1:OT.BinCount
    set(boxH(i),'Color',cols(i,:),'LineWidth', 1+OT.BinDepths(i))
    doplot3(cloud_reg{frame}.Location(OT.PointBins==i,:),'.','Color',cols(i,:))
end
axis image, view(3)


%% Filtering voxels by dimension proportions

% This cell deletes voxels that dont meet a length/height or width/height
% proportion threshold. This threshold can probably be reduced to delete
% more voxels without effecting detections. 


OT.BinBoundaries = OT.BinBoundaries(  (abs(OT.BinBoundaries(:,1)-OT.BinBoundaries(:,4))./ ...
                                  abs(OT.BinBoundaries(:,3)-OT.BinBoundaries(:,6)))  < 10  & ...
                                 (abs(OT.BinBoundaries(:,2)-OT.BinBoundaries(:,5))./ ...
                                  abs(OT.BinBoundaries(:,3)-OT.BinBoundaries(:,6)))  < 10, :);  

% Update voxel count                            
voxel_number = size(OT.BinBoundaries);
OT.BinCount = voxel_number(1);
                              



%% Initialize bin mean and covariance arrays for point-wise voxel merging

% Create container for voxel means
bin_means = zeros(length(OT.BinBoundaries),3);

% Create container for voxel covariances
bin_covs = zeros(3,3,length(OT.BinBoundaries));

for i = 1:OT.BinCount
    
    % Select all points belonging to voxel i
    points = OT.Points((OT.PointBins == i),:);
    
    % Determine size of points array
    point_num = size(points);
    
    if point_num(1) <= 0
        
        % If no points belong to voxel i assign NaN to mean value
        bin_means(i,:) = [NaN,NaN,NaN];
        
        % If no points belong to voxel i assign NaN to covariance
        bin_covs(:,:,i) = [NaN,NaN,NaN;...
                           NaN,NaN,NaN;...
                           NaN,NaN,NaN];
        
    else
        
        % Calculate mean pof voxel points and assign to container
        bin_means(i,:) = sum(points,1)/point_num(1);
        
        % Subtract voxel mean from each voxel point
        bin_normed = points - bin_means(i,:);
        
        % Create intermediate container for each point covariance
        bin_cov = zeros(3,3,point_num(1));
        
        % Calculate covariance for each point
        for j = 1:point_num(1)
            bin_cov(:,:,j) = transpose(bin_normed(j,:))*bin_normed(j,:);
        end
        clear bin_normed
        
        % Sum covariance and normalize to estimate voxel covariance
        bin_covs(:,:,i) = sum(bin_cov,3)/trace(sum(bin_cov,3)); 
        clear bin_cov bin_normed
        
    end

end



%% POINT-WISE FUNCTION ACTIONS

% This cell performs a single merging operation and has worked in the past
% but is currenly throwing errors intermitently. Debugging this cell has
% not been a priority because we have moved on to other methods. 


% Randomly select a voxel index containing points
bin_select = datasample(unique(OT.PointBins),1);
disp(bin_select)
% Find K-nearest neighbors to voxel by voxel means
neighbors = knnsearch(bin_means,bin_means(bin_select,:),'K',11);

% Remove bin_select from neighbor list
neighbors = neighbors(2:11);
disp(neighbors)
for i = 1:length(neighbors)
    
    % Calculate euclidian distance between selected voxel and neighbor
    euclid_dist = sqrt(sum((bin_means(bin_select)-bin_means(neighbors(i))).^2));
    fprintf('euclidian distance is : %d m \n', euclid_dist);
    % Calculate frobenius norm of selected voxel covariance and
    % neighbor voxel covariance
    frob_norm = sqrt(sum(sum((bin_covs(:,:,bin_select) - bin_covs(:,:,neighbors(i))).^2))); 
    fprintf('frobenius norm is : %d \n ', frob_norm);
    if euclid_dist < 0.5 && frob_norm < 0.15
        
        % Retrieving points from selected voxel and neighbor i
        select_points = OT.Points((OT.PointBins == bin_select),:);
        neighbor_points = OT.Points((OT.PointBins == neighbors(i)),:);
        
        % Concatenating points to form merged voxel point array
        new_points = [select_points; neighbor_points];
        
        % Counting number of points in merged voxel
        point_count = size(new_points);
        point_count = point_count(1);
        
        % Calculating merged voxel mean
        mean_update = (sum(select_points,1) + sum(neighbor_points,1))...
                       / point_count;
                   
        % Update voxel means data structure
        bin_means(bin_select,:) = mean_update;
        
        % Calculate covariance
        cov_update = calc_cov(mean_update,new_points);
        
        % Update covariance data structure
        bin_covs(:,:,bin_select) = cov_update;
         
        % Updating PointBins data structure
        OT.PointBins(OT.PointBins == neighbors(i)) = bin_select;

        % Updating BinCount
        OT.BinCount = OT.BinCount - 1;
        
        % Updating BinBoundaries
        OT.BinBoundaries(bin_select,:) = [min(min(OT.BinBoundaries(bin_select,1:2), OT.BinBoundaries(neighbors(i),1:2))), ...
                                          max(max(OT.BinBoundaries(bin_select,1:2), OT.BinBoundaries(neighbors(i),1:2))), ...
                                          min(min(OT.BinBoundaries(bin_select,3:4), OT.BinBoundaries(neighbors(i),3:4))), ...
                                          max(max(OT.BinBoundaries(bin_select,3:4), OT.BinBoundaries(neighbors(i),3:4))), ...
                                          min(min(OT.BinBoundaries(bin_select,5:6), OT.BinBoundaries(neighbors(i),5:6))), ...
                                          max(max(OT.BinBoundaries(bin_select,5:6), OT.BinBoundaries(neighbors(i),5:6)))];
        
    end  
    
end    


%% Testing merge_voxel function

% Testing merge_voxel function which is a recursive, functional form of the
% single merging operation in the cell above. This function has
% successfully run in the past but is now throwing an error. Debugging this
% error has not been a priority because we have shifted focus to detecting
% moving voxels and merging voxels by voxel properties not point
% properties. 

[OT,bin_means,bin_covs] = merge_voxels(OT,bin_means,bin_covs,0);


%% Visualizing results


% Plotting Voxels

figure()
boxH = OT.plot;
cols = lines(OT.BinCount);
doplot3 = @(p,varargin)plot3(p(:,1),p(:,2),p(:,3),varargin{:});
for i = 1:OT.BinCount
    set(boxH(i),'Color',cols(i,:),'LineWidth', 1+OT.BinDepths(i))
    doplot3(cloud_reg{frame}.Location(OT.PointBins==i,:),'.','Color',cols(i,:))
end
axis image, view(3)



%% Showing bin points

% This cell has been used to a selected bin and its neighbors for trouble
% shooting purposes. 

bin1 = OT.Points((OT.PointBins == bin_select),:);
bin2 = OT.Points((OT.PointBins == neighbors(1)),:);
bin3 = OT.Points((OT.PointBins == neighbors(2)),:);
bin4 = OT.Points((OT.PointBins == neighbors(3)),:);
bin5 = OT.Points((OT.PointBins == neighbors(4)),:);
bin6 = OT.Points((OT.PointBins == neighbors(5)),:);
bin7 = OT.Points((OT.PointBins == neighbors(6)),:);
bin8 = OT.Points((OT.PointBins == neighbors(7)),:);
bin9 = OT.Points((OT.PointBins == neighbors(8)),:);
bin10 = OT.Points((OT.PointBins == neighbors(9)),:);
bin11 = OT.Points((OT.PointBins == neighbors(10)),:);

pcshow(cloud_reg{frame}.Location,'cyan'); hold on;
pcshow(bin1,'green'); hold on;
pcshow(bin2,'yellow'); hold on;
pcshow(bin3,'blue'); hold on;
pcshow(bin4,'black'); hold on;
pcshow(bin5,'magenta'); hold on;
pcshow(bin6,'yellow'); hold on;
pcshow(bin7,'magenta'); hold on;
pcshow(bin8,'black'); hold on;
pcshow(bin9,'red'); hold on;
pcshow(bin10,'blue'); hold on;
pcshow(bin11,'red')


