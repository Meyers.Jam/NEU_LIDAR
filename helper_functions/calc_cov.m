function [covariance] = calc_cov(mean,points)

% Subtract voxel mean from each voxel point
normed = points - mean;

% Calculate number of data points
point_num = size(points);
point_num = point_num(1);

% Create intermediate container for each point covariance
cov_container = zeros(3,3,point_num);

% Calculate covariance for each point
for i = 1:point_num
    cov_container(:,:,i) = transpose(normed(i,:))*normed(i,:);
end

% Sum covariances and normalize to estimate voxel covariance
covariance = sum(cov_container,3)/trace(sum(cov_container,3));

end