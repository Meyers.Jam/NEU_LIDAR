function [pcloud_registered] = register_clouds(pcloud_port,pcloud_star)

% Translate port velodyne sensor cloud to port to center objects on
% the centerline
A_port = [1 0 0 0; ...
     0 1 0 0; ...
     0 0 1 0; ...
     0 .6604 0 1];
 
% Translate starboard velodyne sensor cloud to starboard to center objects on
% the centerline
A_star = [1 0 0 0; ...
     0 1 0 0; ...
     0 0 1 0; ...
     0 -.6604 0 1];
 
% Create affine transform objects
tform_port = affine3d(A_port);
tform_star = affine3d(A_star);

% Apply affine transforms to pointsclouds
pcloud_1_registered = cellfun(@(x) pctransform(x,tform_port), pcloud_port,'UniformOutput',false);
pcloud_2_registered = cellfun(@(x) pctransform(x,tform_star), pcloud_star,'UniformOutput',false);


% Merge registered point clouds into a single cloud
pcloud_registered = cellfun(@(x,y) pcmerge(x,y,0.01),pcloud_1_registered,pcloud_2_registered,'UniformOutput',false);

end