%% Example usage of extract_clouds helper function
% Run by section to see how each set of function works!

[cloud_port,cloud_star] = extract_clouds('../data_collection/Stationary_square_2018-03-25-12-06-57.bag');

% NOTES:
% The clouds returned are cell arrays where each element in the array
%     is a Matlab pointCloud object.
%
% This function requires the matlab robotics toolbox
%
% If you get an error saying you are out of java heap space
%     in MATLAB go to: HOME->Preferences->General->Java heap memory
%         use the slide bar to increase java heap memory. 1GB should be
%         more than enough

%% Example usage of extract_clouds helper function with threshold argument passed

[cloud_port,cloud_star] = extract_clouds('../data_collection/Stationary_square_2018-03-25-12-06-57.bag',10);

% NOTES:
% In this example a threshold argument is passed. Extract clouds returns a
%     cell array of Matlab pointCloud objects. All points with X or Y dimensions
%     greater than the threshold value are removed from the cloud. Units
%     are in meters.
%
% In my testing the thresholding works well but occasionally some points are returned
%     that are slightly beyond the threshold. I have not figured out why this is
%     yet. I welcome any feedback on this (and the rest of the code of course).

%% Example usage of extract_clouds helper function with cube threshold argument passed

%Cube of points we are concerned with
%threshold_matrix=[x_min,x_max,y_min,y_max,z_min,z_max];
%threshold_matrix=[8,9;-1,1;-1.5,0.5]; %Based on Stationary Square
threshold_matrix=[-10,10;-10,10;-1.5,10]; %Eliminate Ground Plane (Stationary)

[cloud_port,cloud_star] = extract_clouds('../data_collection/Stationary_square_2018-03-25-12-06-57.bag',10,threshold_matrix);

% NOTES:
% In this example a threshold_matrix argument is passed. Extract clouds returns a
%     cell array of Matlab pointCloud objects. All points with X, Y,or Z dimensions
%     outside the threshold "cube" are removed from the cloud. Units
%     are in meters.
%
%     also, threshold value is left in, but not used for anything. Not the
%     best coding, but it gets the job done!

%% Example usage of register clouds helper function

cloud_reg = register_clouds(cloud_port,cloud_star);

% NOTES:
% The cloud returned is a cell array where each element in the array
%     is a Matlab pointCloud object.
%
% The ordering of the arguments is important here! The port velodyne sensor cloud
%     must be the first argument, the starboard velodyne sensor cloud must be the second
%     argument. This is the same order in which they are output by the
%     extract_clouds function

%% Example plotting a single pointcloud frame
frame = 1;

pcshow(cloud_reg{frame}.Location,'MarkerSize',5)
view(3); %standard view
%view(-90,0); %Perspective of car facing forward

%% Example point cloud player for all frames

delay=0.05; %Decrease to speed up display, Increase to slow down display
play_clouds(cloud_reg,delay);

%% Example Octree display (single frame)

%Addpath to Octree functions
addpath '../ocTree'

%First frame of cloud_reg to OcTree format (weighted into bins 150 pts or
%less)
OT=OcTree(cloud_reg{1,1}.Location,'binCapacity',150,'style','weighted');
OT.shrink;
figure
hold on
plot3(OT);
pcshow(cloud_reg{frame}.Location,'MarkerSize',50)
view(3);

%% Example Octree animated (full rosbag)

%Create array of Octrees from cloud_reg
for n=1:size(cloud_reg,1)
    OT(n)=OcTree(cloud_reg{n,1}.Location,'binCapacity',100,'style','weighted');
    %OT(n).shrink; %Can be uncommented!
end

%WARNING: Cannot get to display at full speed (very slow). To exit, press CTRL+C in
%Command Window
figure
hold on
for n=1:size(cloud_reg,1)
    plot3(OT(n));
    pcshow(cloud_reg{n}.Location,'MarkerSize',50)
    view(3);
    pause(0.01);
    clf;
end

%% Kmeans clustering example

%Imports rosbag data collected from NEU Autonomous Car via extract_clouds
%function, then registers points via register_clouds function. Pointclouds
%are then formed into a designated number of clusters "K" using matlab's
%kmeans function. These clusters are matched from frame to frame to track
%objects over time. This function still lacks flexibility in tracking
%objects for any number of clusters different than K=5

kmeans_lidar
