function [xyz] = crop(xyz,threshold)

    % Filter out xyz points with dimensions greater than threshold
    
    % Find points outside threshold in X
    filter_x = abs(xyz(:,1)) > threshold;
    
    % Find points outside threshold in Y
    filter_y = abs(xyz(:,2)) > threshold;

    % Find points outside threshold in X or Y
    filter = filter_x | filter_y;

    % Remove points outside threshold in X or Y
    xyz(filter,:) = [];

end
