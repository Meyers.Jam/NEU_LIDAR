
%% Extract port and starboard velodyne clouds from bag file in cell array of
% matlab pointCloud objects
[cloud_port,cloud_star] = extract_clouds('../data_collection/NewburyStopped_01_lidar.bag',12);

% Register port and starboard pointclouds 
cloud_reg = register_clouds(cloud_port,cloud_star);

% Freeing memory
clear cloud_port cloud_star

% Define ground plane segmentation parameters
maxDistance = 0.2; % in meters
referenceVector = [0, 0, 1];

% Performing ground plane segmentation, indentifying indices of plane
% outliers
[~, ~, outliers] = cellfun(@(x) pcfitplane(x,maxDistance,referenceVector), cloud_reg,'UniformOutput',false);

% Selecting plane outlier points
select_handle = @select;
cloud_filtered = cellfun(select_handle,cloud_reg,outliers,'UniformOutput',false);

% Freeing memory
clear outliers


%% Identifying moving voxels

% moving_voxels = cell(2,length(cloud_filtered)-1);
moving_voxels = {};


for i = 2:length(cloud_filtered)
% for i = 2:75

    % Create voxels through recursive octatree split method
    OT = OcTree(cloud_filtered{i}.Location,'binCapacity',100);%,'style','weighted'); 

    % Shrink voxels to tightly bound points
    OT.shrink;

    % Create container for voxel-wise count of points
    point_count = zeros(OT.BinCount,1);

    % Count points in each voxel and populate container
    for j = 1:OT.BinCount
    
     point_count(j,1) = sum(OT.PointBins(:,:) == j);
    
    end

    % Remove all non-leaf voxels from OT.BinBoundaries and point_count
    OT.BinBoundaries = OT.BinBoundaries(unique(sort(OT.PointBins)),:);
    point_count = point_count(unique(sort(OT.PointBins)),:);

    % Update the bin count
    OT.BinCount = length(OT.BinBoundaries);
    
    %-----------------------------------------------------------------
    
    % Defining the number of points required for occupancy
    point_threshold = 50;

    % Create container for voxel-wise count of points in previous frame
    point_count_prev = zeros(OT.BinCount,1);

    % Count points of previous frame in voxels from current frame and populate
    % container
    for k = 1:OT.BinCount
    
        voxel = OT.BinBoundaries(k,:);
    
        in_points = cloud_filtered{i-1}.Location(:,1) < max(voxel(1,1),voxel(1,4)) &...
                    cloud_filtered{i-1}.Location(:,1) > min(voxel(1,1),voxel(1,4)) &...
                    cloud_filtered{i-1}.Location(:,2) < max(voxel(1,2),voxel(1,5)) &...
                    cloud_filtered{i-1}.Location(:,2) > min(voxel(1,2),voxel(1,5)) &...
                    cloud_filtered{i-1}.Location(:,3) < max(voxel(1,3),voxel(1,6)) &...
                    cloud_filtered{i-1}.Location(:,3) > min(voxel(1,3),voxel(1,6));
    
        point_count_prev(k,1) = sum(in_points); 
    end

    % Deleting all voxels that do not contain "moving objects", where a voxel
    % containing moving objects is defined as one that has more points than a
    % threshold in the current frame but fewer points than the threshold in the
    % previous frame
    OT.BinBoundaries = OT.BinBoundaries((point_count_prev < point_threshold & point_count > point_threshold),:);
    
    % Deleting voxels with extreme dimension ratios
    OT.BinBoundaries = OT.BinBoundaries(  (abs(OT.BinBoundaries(:,1)-OT.BinBoundaries(:,4))./ ...
                                  abs(OT.BinBoundaries(:,3)-OT.BinBoundaries(:,6)))  < 10  & ...
                                 (abs(OT.BinBoundaries(:,2)-OT.BinBoundaries(:,5))./ ...
                                  abs(OT.BinBoundaries(:,3)-OT.BinBoundaries(:,6)))  < 10, :); 

    % Update the bin count
    bin_num = size(OT.BinBoundaries);
    OT.BinCount = bin_num(1);

    
    moving_voxels{1,i} = OT;
    

end



%% Plotting all voxel frames

% This cell makes a plot of the "moving voxels" for every pointcloud frame.
% It saves thes plots as images do a "cloud_frames" directory, where they
% can be read be read and stitched together into a video with the
% create_video function.

% NOTE: the plots' axis limits are currenlty not constant from plot to plot
% resulting in jitter in the video produced by create_video. This is
% something that needs to be debugged. 

for i = 2:length(moving_voxels)
    
    fig = figure();set(fig, 'Visible', 'off');
    xlim([-13 13]);
    ylim([-13 13]);
    zlim([-2 0.5]);
    
    
    boxH = moving_voxels{1,i}.plot; hold on;
    cols = lines(moving_voxels{1,i}.BinCount);
    doplot3 = @(p,varargin)plot3(p(:,1),p(:,2),p(:,3),varargin{:});
    for j = 1:moving_voxels{1,i}.BinCount
      set(boxH(j),'Color','red','LineWidth', 1+moving_voxels{1,i}.BinDepths(j))
    %     doplot3(moving_voxels{1,40}.Points(moving_voxels{1,40}.PointBins==i,:),'.','Color','red')
    end

    C = ['blue'];
    pcshow(moving_voxels{1,i}.Points,C);
    
%     xlim([-13 13]); hold on;


    axis image, view(3)

    saveas(fig,sprintf('../cloud_frames/frame%f.png',i))
    
    
end 
