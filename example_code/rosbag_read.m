%rosbag_read
%Author: James Meyers
%Date Created: 23MAR18

%Read in rosbag files
SingleBike = rosbag('Single_bike/2017-09-18-20-09-08.bag');
MultiBike = rosbag('Multi_bike/2017-11-02-11-21-27_3.bag');

%Show Available Topics
%SingleBike.AvailableTopics %/velodyne_points

%Show All Messages in rosbag
%SingleBike.MessageList;

%Filter Rosbag by topic name
sb_velodyne_points = select(SingleBike, 'Topic', '/velodyne_points');
mb_velodyne_points = select(MultiBike, 'Topic', '/velodyne_points');

%Return all messages as cell array
sb_msg_velodyne_points = readMessages(sb_velodyne_points);
mb_msg_velodyne_points = readMessages(mb_velodyne_points);