%OcTree examples from https://www.mathworks.com/matlabcentral/fileexchange/40732-octree-partitioning-3d-points-into-spatial-subvolumes
%28MAR18

%% Example 1: Decompose 200 random points into bins of 20 points or less,
% then display each bin with its points in a separate colour.
pts = (rand(200,3)-0.5).^2;
OT = OcTree(pts,'binCapacity',20);
figure
boxH = OT.plot;
cols = lines(OT.BinCount);
doplot3 = @(p,varargin)plot3(p(:,1),p(:,2),p(:,3),varargin{:});
for i = 1:OT.BinCount
    set(boxH(i),'Color',cols(i,:),'LineWidth', 1+OT.BinDepths(i))
    doplot3(pts(OT.PointBins==i,:),'.','Color',cols(i,:))
end
axis image, view(3)

%% Example 2: Decompose 200 random points into bins of 10 points or less,
% shrunk to minimallly encompass their points, then display.
pts = rand(1000,3);
OT = OcTree(pts,'binCapacity',50,'style','weighted');
OT.shrink
figure
boxH = OT.plot;
cols = lines(OT.BinCount);
doplot3 = @(p,varargin)plot3(p(:,1),p(:,2),p(:,3),varargin{:});
for i = 1:OT.BinCount
    set(boxH(i),'Color',cols(i,:),'LineWidth', 1+OT.BinDepths(i))
    doplot3(pts(OT.PointBins==i,:),'.','Color',cols(i,:))
end
axis image, view(3)

%{
OcTree methods:
shrink - Shrink each bin to tightly encompass its children
query - Ask which bins a new set of points belong to.
plot, plot3 - Plots bin bounding boxes to the current axes.

OcTree properties:
Points - The coordinate of points in the decomposition.
PointBins - Indices of the bin that each point belongs to.
BinCount - Total number of bins created.
BinBoundaries - BinCount-by-6 [MIN MAX] coordinates of bin edges.
BinDepths - The # of subdivisions to reach each bin.
BinParents - Indices of the bin that each bin belongs to.
Properties - Name/Val pairs used for creation (see help above)
%}