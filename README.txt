TEAM L.O.D.A.T. - Lidar Object Detection and Tracking
Authors: James Meyers, Kevin Doty, Liu Zhiyang, Brittany Alves
Date Modified: 19APR18

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Overall Goal: Utilize two lidar scanners mounted on the NEU Autonomous Car to track both stationary and moving objects, 
assigning them relative trajectories to the car. Ultimate goal will be mapping positions of car and tracked objects over 
time. Additional goals include removing road surface data through ground plane segmentation.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
About this repo: The code/data in this repo was developed by team LODAT in order to utilize two lidar sensors mounted 
on Northeastern University's Autonomous Car. There is also code created by Antonio Rufo who did his masters thesis on 
bicycle detection at NEU, and example code from mathworks for ocTree examples and testing the "Autonomous Driving" toolbox. 

Folder/File Breakdown

2017-11-02-11-14-17_0/  Everything in this directory was created using Antonio Rufo's code on a dataset in the "multi_bike" 
folder using functions in the "example_code" folder

car_images/ This directory contains useful videos, screenshots, and images for presentation/reference

cloud_frames/ This directory is a container for images created of frame-by-frame moving voxel detections for the purpose
of stitching together into a video.

data_collection/ This directory contains our data collected March 25th 2018
     -Initial_test (test to ensure proper data collection)
     -MassAveDriveRegistered.mat (matlab file of registered points from 
     lidar data collected while driving on Mass Ave. Points of interest 
     include bicylist on right side, many cars, and moving/stopping
     -Stationary_peoplewalkrun (four people walking in circle around stationary
     car, then running around car, and finally a single person running at car!)
     -Stationary_square (Single person holding a diamond shaped carboard sign 
     in front of the stationary car. Person is stationary and moves towards
     car a the end of the dataset)
     -Stationary_squaremore (Single person holding a diamond shapped carboard
     sign in front of car, moving in a cross pattern (back, fwd, right, left)
There are more datasets that include video input from all five cameras on the NEU car camera array, but due to their 
large file size are not stored here. For more info, please email Kevin Doty at Doty.K@husky.neu.edu

example_code/ This directory is purely code developed/used by Antonio Rufo in his bicycle detection work. It was not 
used much for this project, but is useful nonetheless.

helper_functions/ This directory is the main portion of the project. It contains files created by team LODAT to process 
lidar data and attempt tracking methods by kmeans clustering and voxels.
     -automated_driving_examples (Example for Automated Driving toolbox)

     -automated_driving_test (test of Automated Driving toolbox on our data)

     -calc_cov (This is a supplementary function called by point_wise_voxels.m 
      and merge_voxel.m to calculate the covariance of points in a voxel.) 	  

     -compute_voxels (creates/plots voxels of point cloud data)

     -create_video (This is a script found online and lightly edited. It reads
      images from a directory and stiches them together into a video. It is 
      used for the purpose of visualizing object detection results.)

     -crop (crop point cloud data in 2 dimensions X and Y)

     -crop_cube (crop point cloud data in 3 dimensions X, Y, and Z)

     -detection.m
      import lidar data, segment ground plane and ego-vehicle, split Octree by 
      movement, finally merge voxels to get each object 
     
     -example_usage (Demonstrates use of various helper functions)

     -extract_clouds (This function extracts lidar data from rosbags and crops 
      the resulting point clouds at a max x and y distance in meters. The input 
      is an uncompressed rosbag file containing velodyne_points rostopics and a 
      distance in meters. The output is two matlab cell arrays. One array 
      containing the clouds for the port Velodyne sensor, the other containing 
      the clouds for the starboard Velodyne sensor. The individual point clouds 
      are structured as Matlab pointCloud objects. Each point cloud is cropped 
      to have x and y values less than the provided threshold. )

     -kmeans_lidar (Imports rosbag data collected from NEU Autonomous Car via
      extract_clouds function, then registers points via register_clouds 
      function. Pointclouds are then formed into a designated number of clusters 
      "K" using matlab's kmeans function. These clusters are matched from frame 
      to frame to track objects over time. This function still lacks flexibility 
      in tracking objects for any number of clusters different than K=5)

     -merge_voxels (This is a function called by point_wise_voxels to recursively 
      merge voxels by criteria defined w.r.t. the points contained by the voxels. 
      This is a quick and dirty first pass at a merging function. It is currently 
      throwing an error and has been neglected because we have moved on to other 
      object detection methods.)

     -moving_voxels (This is a pipeline for locating objects in a cell array of 
      points clouds using Antonio Rufo's "moving voxel" detection method. No voxel 
      merging steps are performed at this time. A deletion step is performed by 
      filtering out voxels by a dimension ratio threshold. The final cell of the 
      pipeline creates plots of all the "moving voxel" detections and saves them 
      as images in a cloud_frames directory.)

     -OcTree (This is a data structure available on the Matlab file exchange that 
      recursively partitions pointclouds into voxels. It provides the ability to 
      customize the voxelization process and allows for querying the voxel 
      locations of points and visualizing voxels.)

     -play_clouds (plays an array of point cloud structures using pcplayer)

     -point_wise_voxels(This is a preliminary pipeline for detecting objects in 
      a single point cloud using a rules based voxel merging criteria that utilizes 
      the points contained within the voxels. The merging portion of this script 
      is currently throwing errors intermitently. Debugging it has not been a 
      priority as we have decided to focus on other techniques for object detection.)

     -register_clouds (This function registers and merges point clouds generated 
      by the port and starboard Velodyne sensors on the NEU autonomous car. The 
      inputs are cell arrays of Matlab pointCloud objects. The output is a single 
      cell arrary with each cell containing registered and merged Matlab pointCloud 
      objects. The registration is performed with a simple affine translation. This 
      registration assumes the Velodyne sensors have the exact same orientation. 
      Realistically this assumption is not true but it is good enough for the point 
      distances we are considering. This registration could be improved by measuring 
      difference in orientation of the sensors and incorporating a rotation element 
      into the affine transformation.)

ocTree/ Contains code for computing OcTrees as well as some examples

references/ References utilized

utils/ utilities used by Antonio Rufo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Milestones:

00 - Create GitLab Repo for group (https://gitlab.com/Meyers.Jam/NEU_LIDAR)
01 - Collect Datasets using Autonomous car of lidar data from two sensors
     01-01: Car Stationary, Large Carboard Diamond stationary in front of car.
     Status: Completed by Kevin, Liu, and James Sunday 25MAR18
     01-02: Car Stationary, Large Carboard Diamond moving in cross pattern.
     Status: Completed by Kevin, Liu, and James Sunday 25MAR18
     01-03: Car Stationary, Four pedestrians walking in cirlces around car,
     running around car, and one running at car. 
     Status: Completed by Kevin, Liu, and James Sunday 25MAR18
     01-04: Car moving, mutliple datasets of car driving around Boston
     Status: Completed by Kevin, Liu, and James Sunday 25MAR18
02 - Combine our datasets with previous datasets of bicylcle data collected by
     Antonio Rufo
     Status: Completed 26MAR18 by Kevin and James
03 - Process Rosbags in Matlab
     03-01: Matlab function to convert uncompressed rosbag to matlab structure
     Status: Completed 26MAR18 by Kevin
     03-02: Matlab function to convert matlab structure to registered point
     cloud data
     Status: Completed 26MAR18 by Kevin
     03-03: Visualization capabilities in matlab to play point cloud data as
     video.
     Status: Completed by James 28MAR18
04 - All members thoroughly read Antonio's thesis and understand supporting 
     code, data, and background thoery.
     Status: complete
05 - Object Detection with one LIDAR while car is stationary
     05-01: Using data from one lidar, be able to detect stationary object
     Status: N/A, all point cloud data merged now!
     05-02: Single Lidar object detection (single moving object)
     Status: N/A, all point cloud data merged now!
06 - Object Detection with BOTH LIDARs 
     06-01: Stationary Object
     Status: Achieved via Kmeans clustering within very specific parameters!
     06-02: Moving Single Object
     Status: Achieved via Kmeans clustering within very specific parameters!
     06-03: Moving Multiple Objects
     Status: Achieved partially via kmeans clustering and voxels with varying 
     results
     06-04: Driving around Boston
     Status: Not achieved. Would require considerable amount of additional work
07 - Ground Plane Segmentation
     Status: Achieved via RANSAC. Can be improved to exclude sloped ground
08 - Identification of Objects
     08-01: Differentiate between stationary and moving objects
     Status: Achieved via kmeans clustering within very specific parameters!
     08-02: Assign objects relative velocities compared to car
     Status: Not achieved. Would be next step after improving tracking
     reliability of kmeans and voxel methods. 
     08-03: Find actual position of objects in map overtime
     Status: Not achieved. Would require considerable amount of additional work
09 - Make all applications/functions real-time 
     Status: Not achieved. Would require considerable amount of additional work
